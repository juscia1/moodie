package com.moodie.backend.config;

import com.moodie.backend.exception.AlreadyExistsException;
import com.moodie.backend.exception.NotFoundException;
import com.moodie.backend.exception.UnauthorizedException;
import com.moodie.backend.exception.WrongCredentialsException;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExeptionHandlers {

    @ExceptionHandler(AlreadyExistsException.class)
    public ResponseEntity<ValidationMessage> handleAlreadyExistsException(AlreadyExistsException exception) {
        ValidationMessage validationMessage = new ValidationMessage(exception.getField(), exception.getMessage(), ErrorCode.ALREADY_EXISTS);
        return new ResponseEntity<>(validationMessage, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ValidationMessage> handleMethodArgumentNotValidException(MethodArgumentNotValidException exception) {
        String field = exception.getFieldError().getField();
        String message = exception.getBindingResult().getFieldError().getDefaultMessage();
        ValidationMessage validationMessage = new ValidationMessage(field, message, ErrorCode.FIELD_VALIDATION);
        return ResponseEntity.badRequest().body(validationMessage);
    }

    @ExceptionHandler(WrongCredentialsException.class)
    public ResponseEntity<ValidationMessage> handleWrongCredentialsException(WrongCredentialsException exception) {
        ValidationMessage validationMessage = new ValidationMessage(exception.getField(), exception.getMessage(), ErrorCode.WRONG_CREDENTIALS);
        return ResponseEntity.badRequest().body(validationMessage);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ValidationMessage> handleNotFoundException(NotFoundException exception) {
        ValidationMessage validationMessage = new ValidationMessage(exception.getField(), exception.getMessage(), ErrorCode.NOT_FOUND);
        return new ResponseEntity<>(validationMessage, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(UnauthorizedException.class)
    public ResponseEntity<ValidationMessage> handleUnauthorizedException(NotFoundException exception) {
        ValidationMessage validationMessage = new ValidationMessage(exception.getField(), exception.getMessage(), ErrorCode.UNAUTHORIZED);
        return new ResponseEntity<>(validationMessage, HttpStatus.UNAUTHORIZED);
    }

    @Data
    @AllArgsConstructor
    class ValidationMessage {
        private String field;
        private String message;
        private ErrorCode errorCode;
    }

    enum ErrorCode {
        ALREADY_EXISTS,
        FIELD_VALIDATION,
        WRONG_CREDENTIALS,
        NOT_FOUND,
        UNAUTHORIZED,
    }
}
