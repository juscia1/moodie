package com.moodie.backend.controller;

import com.auth0.jwt.JWT;
import com.moodie.backend.dto.Averages;
import com.moodie.backend.dto.MetricDto;
import com.moodie.backend.model.Metric;
import com.moodie.backend.service.MetricService;
import org.javatuples.Triplet;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Stream;

import static com.moodie.backend.security.SecurityConstants.TOKEN_PREFIX;

@RestController
@RequestMapping("/api/metrics")
public class MetricController {

    private MetricService metricService;

    public MetricController(MetricService metricService) {
        this.metricService = metricService;
    }

    @PostMapping
    public ResponseEntity<MetricDto> saveMetric(@RequestBody @Valid MetricDto metricDto, @RequestHeader(name = "Authorization", defaultValue = "") String token){
        int id = Integer.parseInt(JWT.decode(token.replace(TOKEN_PREFIX, "")).getSubject());
        metricDto.setUser_id(id);
        metricService.saveMetric(metricDto);
        return ResponseEntity.ok(metricDto);
    }

    @GetMapping("/{id}")
    public ResponseEntity<MetricDto> getMetric(@PathVariable("id") int id, @RequestHeader(name = "Authorization", defaultValue = "") String token) {
        int userId = Integer.parseInt(JWT.decode(token.replace(TOKEN_PREFIX, "")).getSubject());
        return ResponseEntity.ok(metricService.getMetric(userId, id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<MetricDto> updateMetric(@PathVariable("id") int id, @RequestHeader(name = "Authorization", defaultValue = "") String token, @RequestBody @Valid MetricDto metricDto) {
        int userId = Integer.parseInt(JWT.decode(token.replace(TOKEN_PREFIX, "")).getSubject());
        metricDto.setUser_id(userId);
        metricDto.setId(id);
        return ResponseEntity.ok(metricService.updateMetric(metricDto));
    }

    @GetMapping
    public ResponseEntity<List<Metric>> getMetrics(@RequestHeader(name = "Authorization", defaultValue = "") String token){
        int id = Integer.parseInt(JWT.decode(token.replace(TOKEN_PREFIX, "")).getSubject());
        return ResponseEntity.ok(metricService.getMetrics(id));
    }

    @GetMapping("/submitted")
    public ResponseEntity<Boolean> userSubmittedToday(@RequestHeader(name = "Authorization", defaultValue = "") String token){
        int id = Integer.parseInt(JWT.decode(token.replace(TOKEN_PREFIX, "")).getSubject());
        return ResponseEntity.ok(metricService.userSubmittedToday(id));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<MetricDto> deleteMetric(@PathVariable("id") int id, @RequestHeader(name = "Authorization", defaultValue = "") String token){
        int userId = Integer.parseInt(JWT.decode(token.replace(TOKEN_PREFIX, "")).getSubject());
        return ResponseEntity.ok(metricService.deleteMetric(id, userId));
    }

    @GetMapping("/progress")
    public ResponseEntity<Stream<?>> getAllMetrics(@RequestHeader(name = "Authorization", defaultValue = "") String token, @RequestHeader(name="Range", defaultValue = "week") String range){
        int userId = Integer.parseInt(JWT.decode(token.replace(TOKEN_PREFIX, "")).getSubject());
        LocalDate now = LocalDate.now();
        LocalDate from, to;
        if (range.equals("month")) {
            from = now.withDayOfMonth(1);
        } else {
            from = now.with(DayOfWeek.MONDAY);
        }
        to = now;
        return ResponseEntity.ok(metricService.getMetrics(userId, from, to));
    }

    @GetMapping("/averages")
    public ResponseEntity<Triplet<Averages, Averages, List<String>>> getAverages(@RequestHeader(name = "Authorization", defaultValue = "") String token){
        int userId = Integer.parseInt(JWT.decode(token.replace(TOKEN_PREFIX, "")).getSubject());

        LocalDate to = LocalDate.now();
        LocalDate from = LocalDate.now().with((DayOfWeek.MONDAY));

        Averages weekAverages = metricService.getAverages(userId, from, to);
        from = LocalDate.now().withDayOfMonth(1);
        Averages monthAverages = metricService.getAverages(userId, from, to);
        List<String> warings = metricService.getwWarnings(weekAverages,monthAverages);

        return ResponseEntity.ok(Triplet.with(weekAverages,monthAverages, warings));
    }

}
