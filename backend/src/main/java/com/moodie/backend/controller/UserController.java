package com.moodie.backend.controller;


import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.moodie.backend.dto.AuthRequestDto;
import com.moodie.backend.dto.LoggedInUserDto;
import com.moodie.backend.dto.UserDto;
import com.moodie.backend.dto.UserProfileDto;
import com.moodie.backend.exception.AlreadyExistsException;
import com.moodie.backend.exception.WrongCredentialsException;
import com.moodie.backend.model.User;
import com.moodie.backend.model.UserInfo;
import com.moodie.backend.service.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;

import static com.moodie.backend.security.SecurityConstants.*;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private UserService userService;
    private AuthenticationManager authenticationManager;

    public UserController(UserService userService, AuthenticationManager authenticationManager) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
    }

    @PostMapping("/sign-up")
    ResponseEntity registerUser(@RequestBody @Valid UserDto userDto) {
        userService.registerUser(userDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/sign-in")
    ResponseEntity<LoggedInUserDto> login(@RequestBody @Valid AuthRequestDto authRequestDto, HttpServletResponse response) {
        try {
            Authentication authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(
                            authRequestDto.getUsername(),
                            authRequestDto.getPassword(),
                            new ArrayList<>()
                    ));

            String refreshToken = REFRESH_TOKEN_PREFIX + createRefreshToken(authentication);
            Cookie cookie = new Cookie("refreshToken", refreshToken);
            cookie.setHttpOnly(true);
            //cookie.setSecure(true);
            cookie.setPath("/");
            response.addCookie(cookie);

            String token = TOKEN_PREFIX + createJWT(authentication);
            HttpHeaders headers = new HttpHeaders();
            headers.add(HEADER_STRING, token);
            headers.add("Access-Control-Expose-Headers", "*");


            LoggedInUserDto loggedInUserDto = new LoggedInUserDto(
                    ((UserInfo) authentication.getPrincipal()).getId(),
                    authRequestDto.getUsername()
            );
            return new ResponseEntity<>(loggedInUserDto, headers, HttpStatus.OK);
        } catch (AuthenticationException e) {
            throw new WrongCredentialsException("Wrong username or password.", "Login");
        }
    }

    @PostMapping("/refresh")
    public ResponseEntity<?> refreshToken(@CookieValue(value = "refreshToken", defaultValue = "") String refreshToken) {
        if (!refreshToken.equals("") && refreshToken.startsWith(REFRESH_TOKEN_PREFIX)) {
            try {
                String id = JWT.decode(refreshToken.replace(REFRESH_TOKEN_PREFIX, "")).getSubject();
                String newToken = TOKEN_PREFIX + JWT.create()
                        .withSubject(id)
                        .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                        .sign(Algorithm.HMAC256(SECRET.getBytes()));

                HttpHeaders headers = new HttpHeaders();
                headers.add(HEADER_STRING, newToken);
                headers.add("Access-Control-Expose-Headers", HEADER_STRING);
                return new ResponseEntity<>(headers, HttpStatus.OK);
            } catch (JWTDecodeException e) {
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
        }
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }

    @PostMapping("/log-out")
    public ResponseEntity<?> logout(HttpServletResponse response) {
        try {
            Cookie cookie = new Cookie("refreshToken", null);
            cookie.setMaxAge(0);
            //cookie.setSecure(true);
            cookie.setHttpOnly(true);
            cookie.setPath("/");

            response.addCookie(cookie);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/profile")
    public ResponseEntity<UserProfileDto> getProfile(@RequestHeader(name = "Authorization", defaultValue = "") String token) {
        try {
            if (token.startsWith(TOKEN_PREFIX) && !token.equals("")) {
                int id = Integer.parseInt(JWT.decode(token.replace(TOKEN_PREFIX, "")).getSubject());
                UserProfileDto user = userService.getUserProfile(id);
                return ResponseEntity.ok(user);
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(null);
        }
    }

    @PostMapping("/profile")
    public ResponseEntity<UserProfileDto> updateProfile(@RequestHeader(name = "Authorization", defaultValue = "") String token, @RequestBody @Valid UserProfileDto user) {
        try {
            if (token.startsWith(TOKEN_PREFIX) && !token.equals("")) {
                int id = Integer.parseInt(JWT.decode(token.replace(TOKEN_PREFIX, "")).getSubject());
                user.setId(id);
                UserProfileDto result = userService.updateUser(user);
                if (result != null) {
                    return ResponseEntity.ok(result);
                } else {
                    return ResponseEntity.badRequest().body(null);
                }
            } else {
                return ResponseEntity.badRequest().body(null);
            }
        } catch (AlreadyExistsException e) {
            throw e;
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(null);
        }
    }

    @DeleteMapping("")
    public ResponseEntity<?> deleteUser(@RequestHeader(name = "Authorization", defaultValue = "") String token, HttpServletResponse response) {
        try {
            if (token.startsWith(TOKEN_PREFIX) && !token.equals("")) {
                int id = Integer.parseInt(JWT.decode(token.replace(TOKEN_PREFIX, "")).getSubject());
                userService.deleteUser(id);
                Cookie cookie = new Cookie("refreshToken", null);
                cookie.setMaxAge(0);
                //cookie.setSecure(true);
                cookie.setHttpOnly(true);
                cookie.setPath("/");

                response.addCookie(cookie);
                return new ResponseEntity<>(HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    private String createJWT(Authentication authentication) {
        return JWT.create()
                .withSubject(Integer.toString(((UserInfo) authentication.getPrincipal()).getId()))
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .sign(Algorithm.HMAC256(SECRET.getBytes()));
    }

    private String createRefreshToken(Authentication authentication) {
        return JWT.create()
                .withSubject(Integer.toString(((UserInfo) authentication.getPrincipal()).getId()))
                .withExpiresAt(new Date(System.currentTimeMillis() + REFRESH_TOKEN_EXPIRATION))
                .sign(Algorithm.HMAC256(SECRET.getBytes()));
    }
}
