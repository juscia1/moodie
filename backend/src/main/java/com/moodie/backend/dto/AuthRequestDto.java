package com.moodie.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
public class AuthRequestDto {
    @NotBlank
    private String username;
    @NotBlank
    private String password;
}
