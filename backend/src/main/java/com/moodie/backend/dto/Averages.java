package com.moodie.backend.dto;

public interface Averages {
    Float getMood();
    Float getSleepHours();
}
