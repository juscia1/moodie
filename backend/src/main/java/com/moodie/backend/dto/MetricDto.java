package com.moodie.backend.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.moodie.backend.model.Metric;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MetricDto {
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;
    @NotNull
    @Max(value = 10)
    @Min(value = 0)
    private short mood;
    @NotNull
    @Max(value = 24)
    @Min(value = 0)
    private short sleepHours;
    private String notes;
    private int user_id;
    private int id;

    public MetricDto(LocalDate date, @NotNull @Max(value = 10) @Min(value = 1) short mood, @NotNull @Max(value = 24) @Min(value = 0) short sleepHours, String notes) {
        this.date = date;
        this.mood = mood;
        this.sleepHours = sleepHours;
        this.notes = notes;
    }

    public MetricDto(@NotNull @Max(value = 10) @Min(value = 1) short mood, @NotNull @Max(value = 24) @Min(value = 0) short sleepHours, String notes) {
        this.mood = mood;
        this.sleepHours = sleepHours;
        this.notes = notes;
    }

    public MetricDto(Metric metric, int userId){
        this.date = metric.getDate();
        this.mood = metric.getMood();
        this.sleepHours = metric.getSleepHours();
        this.notes = metric.getNotes();
        this.user_id = userId;
    }
}
