package com.moodie.backend.dto;

import com.moodie.backend.model.Metric;
import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
public class MetricProgressDto {
    private String date;
    private short mood;
    private short sleepHours;

    public MetricProgressDto(Metric metric) {
        this.date = metric.getDate().getMonth().toString() + " " + metric.getDate().getDayOfMonth();
        this.mood = metric.getMood();
        this. sleepHours = metric.getSleepHours();
    }
}
