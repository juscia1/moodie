package com.moodie.backend.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.moodie.backend.validator.Password;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.*;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    @NotBlank
    @Size( max = 255)
    private String username;
    @NotBlank
    @Email
    @Size( max = 255)
    private String email;
    @NotBlank
    @Size( max = 255)
    @Password
    private String password;
    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthdate;

    public UserDto(String username, String email) {
        this.username = username;
        this.email = email;
    }
}
