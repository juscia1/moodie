package com.moodie.backend.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Data
@NoArgsConstructor
public class UserProfileDto {
    private int id;
    @NotBlank
    @Size(max = 255)
    private String username;
    @NotBlank
    @Email
    @Size(max = 255)
    private String email;
    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthdate;

    public UserProfileDto(String username, String email, LocalDate birthdate) {
        this.username = username;
        this.email = email;
        this.birthdate = birthdate;
    }
}
