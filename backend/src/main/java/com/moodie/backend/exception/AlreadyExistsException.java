package com.moodie.backend.exception;

import lombok.Data;

@Data
public class AlreadyExistsException extends RuntimeException{
    private final String field;

    public AlreadyExistsException(String message, String field) {
        super(message);
        this.field = field;
    }
}
