package com.moodie.backend.exception;

public class UnauthorizedException extends RuntimeException {
    private final String field;

    public UnauthorizedException(String message, String field) {
        super(message);
        this.field = field;
    }
}
