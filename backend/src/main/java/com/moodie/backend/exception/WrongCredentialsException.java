package com.moodie.backend.exception;

import lombok.Data;

@Data
public class WrongCredentialsException extends RuntimeException{
    private final String field;

    public WrongCredentialsException(String message, String field) {
        super(message);
        this.field = field;
    }
}
