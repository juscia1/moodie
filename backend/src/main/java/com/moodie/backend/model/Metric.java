package com.moodie.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "metric")
@Getter
@Setter
@NoArgsConstructor
public class Metric {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private LocalDate date;
    private short mood;
    @Column(name = "sleep_hours")
    private short sleepHours;
    private String notes;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="user_id")
    @NotNull
    @JsonIgnore
    private User user;

    public Metric(short mood, short sleepHours, String notes) {
        this.mood = mood;
        this.sleepHours = sleepHours;
        this.notes = notes;
    }
}
