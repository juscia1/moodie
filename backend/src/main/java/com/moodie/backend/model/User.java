package com.moodie.backend.model;

import com.moodie.backend.dto.UserDto;
import com.moodie.backend.dto.UserProfileDto;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "users")
@Getter
@Setter
@NoArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique = true)
    private String username;
    @Column(unique = true)
    private String email;
    private String password;
    private LocalDate birthdate;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "user")
    private Set<Metric> metrics;

    public User(String username, String email, String password, LocalDate birthdate) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.birthdate = birthdate;
    }

    public User(UserDto userDto) {
        this.username = userDto.getUsername();
        this.email = userDto.getEmail();
        this.password = userDto.getPassword();
        this.birthdate = userDto.getBirthdate();
    }

    public boolean equals(UserProfileDto user) {
        if (this.id == user.getId() &&
                this.username.equals(user.getUsername()) &&
                this.email.equals(user.getEmail()) &&
                this.birthdate.equals(user.getBirthdate())) return true;
        return false;
    }
}
