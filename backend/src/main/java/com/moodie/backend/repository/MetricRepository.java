package com.moodie.backend.repository;

import com.moodie.backend.dto.Averages;
import com.moodie.backend.model.Metric;
import org.postgresql.core.Tuple;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.util.Pair;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface MetricRepository extends JpaRepository<Metric, Integer> {

    @Query(value = "SELECT * FROM metric\n" +
                   "WHERE metric.user_id = :id\n" +
                   "ORDER BY metric.date DESC", nativeQuery = true)
    List<Metric> findAllSubmits(@Param("id") int id);

    @Query(value = "SELECT * FROM metric\n" +
                   "WHERE metric.user_id = :userId AND metric.date = :date", nativeQuery = true)
    Optional<Metric> findMetricByDate(@Param("userId") int userId, @Param("date") LocalDate date);

    @Query(value = "SELECT COUNT(*) from metric\n" +
                   "WHERE metric.user_id = :userId AND metric.date = :date", nativeQuery = true)
    int countMetricByDate(@Param("userId") int userId, @Param("date") LocalDate date);

    @Query(value = "SELECT * FROM metric\n" +
                   "WHERE metric.user_id = :userId AND metric.date >= :from AND metric.date <= :to\n" +
                   "ORDER BY metric.date ASC",
                    nativeQuery = true)
    List<Metric> getAllSubmits(@Param("userId") int userId, @Param("from") LocalDate from, @Param("to") LocalDate to);

    @Query(value = "SELECT * FROM metric\n" +
                   "WHERE metric.user_id = :userId AND metric.id = :id", nativeQuery = true)
    Optional<Metric> getMetric(@Param("userId") int userId, @Param("id") int id);

    @Query(value = "SELECT COALESCE(AVG(metric.mood),0) AS mood, COALESCE(AVG(metric.sleep_hours),0) AS sleepHours\n" +
                   "FROM metric\n" +
                   "WHERE metric.user_id = :userId AND metric.date >= :from AND metric.date <= :to",
                    nativeQuery = true)
    Averages getAverages(@Param("userId") int userId, @Param("from") LocalDate from, @Param("to") LocalDate to);


}
