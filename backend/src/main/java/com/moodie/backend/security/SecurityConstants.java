package com.moodie.backend.security;

public class SecurityConstants {
    public static final String SECRET = "QfTjWnZr4u7x!A%D*G-KaPdSgUkXp2s5";
    public static final long EXPIRATION_TIME = 900_000; // 15 mins
    public static final long REFRESH_TOKEN_EXPIRATION = 1000 * 60 * 60 * 24 * 7;
    public static final String REFRESH_TOKEN_PREFIX = "Bearer_";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
}
