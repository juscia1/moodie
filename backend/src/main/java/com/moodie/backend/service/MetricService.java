package com.moodie.backend.service;

import com.moodie.backend.dto.Averages;
import com.moodie.backend.dto.MetricDto;
import com.moodie.backend.dto.MetricProgressDto;
import com.moodie.backend.exception.AlreadyExistsException;
import com.moodie.backend.exception.NotFoundException;
import com.moodie.backend.exception.UnauthorizedException;
import com.moodie.backend.model.Metric;
import com.moodie.backend.repository.MetricRepository;
import com.moodie.backend.repository.UserRepository;
import org.postgresql.core.Tuple;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Service
public class MetricService {
    private MetricRepository metricRepository;
    private UserRepository userRepository;

    public MetricService(MetricRepository metricRepository, UserRepository userRepository) {
        this.metricRepository = metricRepository;
        this.userRepository = userRepository;
    }

    public MetricDto saveMetric(MetricDto metricDto) {
        Metric metric = new Metric(
                metricDto.getMood(),
                metricDto.getSleepHours(),
                metricDto.getNotes()
        );
        metric.setDate(LocalDate.now());
        metricDto.setDate(metric.getDate());
        if (metricRepository.findMetricByDate(metricDto.getUser_id(), metricDto.getDate()).isPresent()) {
            throw new AlreadyExistsException(String.format("User has already submited his mood on %b", metricDto.getDate()), "Metric");
        }
        metric.setUser(userRepository.findById(metricDto.getUser_id()).get());
        if (metric.getNotes().equals("")) {
            metric.setNotes(null);
        }
        metricRepository.save(metric);
        return metricDto;
    }

    public MetricDto deleteMetric(int id, int userId) throws NotFoundException {
        Optional<Metric> metric = metricRepository.findById(id);

        if (metric.isEmpty()) {
            throw new NotFoundException(String.format("metric with id = %d not found", id), "metric");
        }

        if (metric.get().getUser().getId() != userId) {
            throw new UnauthorizedException("User unauthorized to delete metric", "metric");
        }

        metricRepository.deleteById(id);

        return new MetricDto(metric.get(), userId);
    }

    public List<Metric> getMetrics(int id) {
        return metricRepository.findAllSubmits(id);
    }

    public Stream<?> getMetrics(int id, LocalDate from, LocalDate to) {
        return metricRepository.getAllSubmits(id, from, to).stream().map(metric -> new MetricProgressDto(metric));
    }

    public MetricDto getMetric(int userId, int id) {
        Metric metric = metricRepository.getMetric(userId, id).orElse(null);
        if (metric == null) {
            throw new NotFoundException("Metric not found", "metric");
        }
        return new MetricDto(metric.getDate(), metric.getMood(), metric.getSleepHours(), metric.getNotes(), userId, id);
    }

    public MetricDto updateMetric(MetricDto metricDto) {
        Metric metric = metricRepository.getMetric(metricDto.getUser_id(), metricDto.getId()).orElse(null);
        if (metric == null) {
            throw new NotFoundException("Metric not found", "metric");
        }
        if (metric.getUser().getId() != metricDto.getUser_id()) {
            throw new UnauthorizedException("User unauthorized to edit metric", "metric");
        }

        metric.setMood(metricDto.getMood());
        metric.setSleepHours(metricDto.getSleepHours());
        metric.setNotes(metricDto.getNotes());
        if (metric.getNotes().equals("")) {
            metric.setNotes(null);
        }

        metricRepository.save(metric);

        return metricDto;
    }

    public Averages getAverages(int id, LocalDate from, LocalDate to) {
        return metricRepository.getAverages(id, from, to);
    }

    public ArrayList<String> getwWarnings(Averages week, Averages month) {
        ArrayList<String> warnings = new ArrayList<String>();
        if (week.getMood() <= 5) {
            warnings.add("Your mood's been low this week. Is everything all right?");
        }
        if (week.getSleepHours() < 7) {
            warnings.add("You're not getting enough sleep this week.");
        } else if (week.getSleepHours() > 9) {
            warnings.add("You've been sleeping a bit too much this week.");
        }
        if (month.getMood() <= 5) {
            warnings.add("Your mood's been low this month. Is everything all right?");
        }
        if (month.getSleepHours() < 7) {
            warnings.add("You're not getting enough sleep this month.");
        } else if (month.getSleepHours() > 9) {
            warnings.add("You've been sleeping a bit too much this month.");
        }
        return warnings;
    }

    public boolean userSubmittedToday(int id) {
        return metricRepository.countMetricByDate(id, LocalDate.now()) != 0;
    }
}
