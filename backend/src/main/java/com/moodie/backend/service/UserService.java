package com.moodie.backend.service;

import com.moodie.backend.dto.UserDto;
import com.moodie.backend.dto.UserProfileDto;
import com.moodie.backend.exception.AlreadyExistsException;
import com.moodie.backend.exception.WrongCredentialsException;
import com.moodie.backend.model.User;
import com.moodie.backend.model.UserInfo;
import com.moodie.backend.repository.MetricRepository;
import com.moodie.backend.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {
    private UserRepository userRepository;
    private MetricRepository metricRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserService(UserRepository userRepository, MetricRepository metricRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.metricRepository = metricRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public User registerUser(@Valid UserDto userDto) {
        checkIfTaken(userDto);
        User user = new User(userDto);
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        return user;
    }

    private void checkIfTaken(UserDto userDto) {
        Optional<User> user = userRepository.findUserByUsernameOrEmail(userDto.getUsername(), userDto.getEmail());
        if (user.isPresent()) {
            if (user.get().getUsername().equals(userDto.getUsername())) {
                throw new AlreadyExistsException("Username already taken", "username");
            }
            throw new AlreadyExistsException("Email already taken", "email");
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findUserByUsername(username);
        if (user.isPresent()) {
            return new UserInfo(
                    user.get().getUsername(),
                    user.get().getPassword(),
                    new ArrayList<>(),
                    user.get().getId()
            );
        } else {
            throw new WrongCredentialsException("Wrong username or password.", "Login");
        }
    }

    public UserProfileDto getUserProfile(int id) {
        User user = userRepository.findById(id).orElse(null);
        if (user != null) {
            return new UserProfileDto(user.getUsername(), user.getEmail(), user.getBirthdate());
        } else {
            return null;
        }
    }

    public UserProfileDto updateUser(UserProfileDto userProfileDto) {
        User user = userRepository.findById(userProfileDto.getId()).orElse(null);


        if (user != null) {
            if (user.equals(userProfileDto)) {
                return userProfileDto;
            }

            User checkUsername = userRepository.findUserByUsername(userProfileDto.getUsername()).orElse(null);
            if (checkUsername != null && checkUsername.getUsername().equals(userProfileDto.getUsername()) && checkUsername.getId() != userProfileDto.getId()) {
                throw new AlreadyExistsException("Username already exists", "username");
            }
            User checkEmail = userRepository.findUserByEmail(userProfileDto.getEmail()).orElse(null);
            if (checkEmail != null && checkEmail.getEmail().equals(userProfileDto.getEmail()) && checkEmail.getId() != userProfileDto.getId()) {
                throw new AlreadyExistsException("Email already exists", "email");
            }

            user.setUsername(userProfileDto.getUsername());
            user.setEmail(userProfileDto.getEmail());
            user.setBirthdate(userProfileDto.getBirthdate());

            userRepository.save(user);
            return userProfileDto;
        } else {
            return null;
        }
    }

    public void deleteUser(int id) {
        userRepository.deleteById(id);
    }
}
