package com.moodie.backend.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Constraint(validatedBy = {PasswordValidator.class})
@Target({ ElementType.TYPE, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Password {

    String message() default "Password must contain at least 1 lowercase, 1 uppercase, and 1 numeric character and must be at least 8 characters or longer";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
