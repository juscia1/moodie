CREATE TABLE users (
                        id SERIAL NOT NULL,
                        username VARCHAR (255) NOT NULL UNIQUE,
                        email VARCHAR (255) NOT NULL UNIQUE,
                        password VARCHAR (255) NOT NULL,
                        birthdate DATE NOT NULL,
                        PRIMARY KEY(id)
);

CREATE TABLE metric (
                        id SERIAL NOT NULL,
                        "date" DATE NOT NULL,
                        mood SMALLINT NOT NULL,
                        sleep_hours DECIMAL(4,2) NOT NULL,
                        notes TEXT,
                        PRIMARY KEY(id),
                        user_id INTEGER NOT NULL REFERENCES "users"(id),
                        CHECK (mood > 0 AND mood <=10),
                        CHECK (sleep_hours >= 0 AND sleep_hours<=24)
);
