ALTER TABLE metric
DROP CONSTRAINT metric_mood_check,
ADD CONSTRAINT metric_mood_check
CHECK (mood >= 0 AND mood <= 10);