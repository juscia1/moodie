import { CssBaseline, MuiThemeProvider } from "@material-ui/core";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import AppRouter from "./AppRouter";
import NavBar from "./components/NavBar";
import theme from "./theme";
import refresh from "./utils/refresh";

function App() {
  const dispatch = useDispatch();
  const { token, loading } = useSelector((state) => state);
  useEffect(() => {
    if (!token) {
      refresh(dispatch);
    }
  }, [token]);
  return (
    <MuiThemeProvider theme={theme}>
      <CssBaseline />
      {loading ? <NavBar /> : <AppRouter />}
    </MuiThemeProvider>
  );
}

export default App;
