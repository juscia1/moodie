import React from "react";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import Layout from "./Layout";
import Login from "./Pages/Login";
import Register from "./Pages/Register";

const AppRouter = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/login" component={Login} />
        <Route excat path="/register" component={Register} />
        <Route path="/">
          <Layout />
        </Route>
      </Switch>
    </Router>
  );
};

export default AppRouter;
