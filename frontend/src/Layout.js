import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import NavBar from "./components/NavBar";
import Routes from "./Routes";

const Layout = () => {
  const { push } = useHistory();
  const token = useSelector((state) => state.token);
  useEffect(() => {
    if (token !== 0 && token === null) {
      push("/login");
    }
  }, [token]);
  return (
    <div>
      <header>
        <nav>
          <NavBar />
        </nav>
      </header>

      <main>
        <Routes />
      </main>
    </div>
  );
};

export default Layout;
