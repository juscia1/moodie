import React, { useEffect, useState } from "react";
import {
  Button,
  CircularProgress,
  Grid,
  Hidden,
  Link,
  TextField,
  Typography,
} from "@material-ui/core";
import * as yup from "yup";
import { Form, Formik } from "formik";
import { Redirect, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import useStyles from "./useStyles";
import { API_URL } from "../../config/api";
import { setUser } from "../../state/actions/user";
import { setToken } from "../../state/actions/token";
import logo from "./logo.png";

const Login = () => {
  const [submitError, setSubmitError] = useState("");
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [successLogin, setSuccessLogin] = useState(false);
  const classes = useStyles();
  const dispatch = useDispatch();
  const { token, loading } = useSelector((state) => state);
  const { push } = useHistory();

  useEffect(() => {
    if (token) {
      push("/");
    }
  }, [token]);

  const validationSchema = yup.object({
    username: yup
      .string()
      .required("Username is required")
      .trim("Username cannot be blank")
      .strict(),
    password: yup.string().required("Password is required"),
  });

  const initialValues = {
    username: "",
    password: "",
  };

  const attemptLogin = (values) => {
    setSubmitError("");
    setIsSubmitting(true);
    const url = `${API_URL}/api/users/sign-in`;
    const options = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(values),
    };
    fetch(url, options)
      .then((response) => {
        if (!response.ok) {
          response.json().then((data) => {
            setSubmitError(data.message);
          });
        } else {
          dispatch(setToken(response.headers.get("Authorization")));
          response
            .json()
            .then((data) => {
              dispatch(setUser(data));
            })
            .then(setSuccessLogin(true));
        }
        setIsSubmitting(false);
      })
      .catch(setSubmitError("Something went wrong. Please try again"));
  };

  if (loading) return <div />;
  return (
    <Grid container component="main" className={classes.root}>
      {successLogin ? (
        <Redirect to="/" />
      ) : (
        <>
          <Hidden only={["xs", "sm"]}>
            <Grid item md={6}>
              <div className={classes.side}>
                <div className={classes.container}>
                  <Typography variant="h4">DID YOUR MOOD DIE?</Typography>
                  <img className={classes.image} src={logo} alt="logo" />
                  <Typography variant="h4">
                    USE <span style={{ color: "#9c27b0" }}>MOODIE</span> AND
                    FIND OUT!
                  </Typography>
                </div>
              </div>
            </Grid>
          </Hidden>
          <Grid item xs={12} sm={9} md={6}>
            <div className={classes.paper}>
              <Typography variant="h4">
                WELCOME TO <span style={{ color: "#9c27b0" }}>MOODIE!</span>
              </Typography>
              <Formik
                enableReinitialize
                validateOnChange={false}
                validateOnBlur={false}
                initialValues={initialValues}
                validationSchema={validationSchema}
                onSubmit={(values) => attemptLogin(values)}
              >
                {({ values, errors, handleChange }) => (
                  <Form className={classes.form}>
                    <TextField
                      variant="outlined"
                      margin="normal"
                      className={classes.textField}
                      id="username"
                      label="Username"
                      name="username"
                      value={values.username}
                      error={!!errors.username}
                      helperText={errors.username}
                      onChange={handleChange}
                    />
                    <TextField
                      variant="outlined"
                      margin="normal"
                      className={classes.textField}
                      id="password"
                      label="Password"
                      name="password"
                      type="password"
                      value={values.password}
                      error={!!errors.password}
                      helperText={errors.password}
                      onChange={handleChange}
                    />
                    {isSubmitting ? (
                      <div>
                        <CircularProgress className={classes.submit} />
                      </div>
                    ) : (
                      <>
                        <Button
                          type="submit"
                          variant="outlined"
                          color="primary"
                          className={classes.submit}
                        >
                          SIGN IN
                        </Button>
                        {submitError ? (
                          <div className={classes.error}>{submitError}</div>
                        ) : null}
                      </>
                    )}
                  </Form>
                )}
              </Formik>
              <Typography>
                {"NEW TO MOODIE? "}
                <Link href="/register">CREATE AN ACCOUNT!</Link>
              </Typography>
            </div>
          </Grid>
        </>
      )}
    </Grid>
  );
};
export default Login;
