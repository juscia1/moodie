import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh",
    alignItems: "center",
    display: "flex",
    justifyContent: "center",
  },
  paper: {
    margin: theme.spacing(4, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    textAlign: "center",
  },
  side: {
    margin: theme.spacing(1, 4),
    display: "flex",
    flexDirection: "column",
    textAlign: "center",
    justifyContent: "center",
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    margin: theme.spacing(2, 0, 0),
  },
  submit: {
    margin: theme.spacing(2, "auto"),
    width: "50%",
  },
  spin: {
    margin: "auto",
  },
  error: {
    color: "red",
  },
  textField: {
    [theme.breakpoints.down("md")]: {
      width: "100%",
    },
    [theme.breakpoints.up("md")]: {
      width: "70%",
    },
  },
  image: {
    width: "45%",
    margin: theme.spacing(1),
  },
  text: {
    paddingTop: "7px",
    paddingLeft: "64px",
    position: "absolute",
  },
  container: {
    position: "relative",
  },
}));

export default useStyles;
