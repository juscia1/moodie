import React, { useEffect, useState } from "react";
import {
  Typography,
  Button,
  TextField,
  CircularProgress,
} from "@material-ui/core";
import { Form, Formik } from "formik";
import Icon from "@material-ui/icons/PersonOutlineOutlined";
import * as yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import useStyles from "./useStyles";
import today from "../../utils/today";
import { API_URL } from "../../config/api";
import { setUserUsername } from "../../state/actions/user";
import Loader from "../../components/Loader";

const EditProfile = () => {
  const classes = useStyles();
  const { token } = useSelector((state) => state);
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [birthdate, setBirthdate] = useState("");
  const [takenUsername, setTakenUsername] = useState("");
  const [takenEmail, setTakenEmail] = useState("");
  const [submitError, setSubmitError] = useState("");
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [loading, setLoading] = useState(false);
  const history = useHistory();
  const dispatch = useDispatch();

  const requestData = () => {
    setLoading(true);
    const url = `${API_URL}/api/users/profile`;
    const options = {
      method: "GET",
      headers: {
        "content-type": "application/json",
        Authorization: token,
      },
    };
    if (token !== 0) {
      fetch(url, options)
        .then((res) => res.json())
        .then((data) => {
          setUsername(data.username);
          setEmail(data.email);
          setBirthdate(data.birthdate);
          setLoading(false);
        });
    }
  };

  useEffect(() => {
    requestData();
  }, []);

  const validationSchema = yup.object({
    username: yup
      .string()
      .required("Username is required")
      .trim("Username cannot be blank")
      .strict(),
    email: yup
      .string()
      .email("Email must be a valid email")
      .required("Email is required")
      .strict(),
    birthdate: yup.date().required("Birthdate is required").nullable(),
  });

  const initialValues = {
    username,
    email,
    birthdate,
  };

  const attemptUpdate = (values) => {
    setIsSubmitting(true);
    const url = `${API_URL}/api/users/profile`;
    const options = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "content-type": "application/json",
        Authorization: token,
      },
      body: JSON.stringify(values),
    };
    if (token !== 0) {
      fetch(url, options)
        .then((response) => {
          if (!response.ok) {
            response.json().then((data) => {
              if (data.field === "username") {
                setTakenUsername(data.message);
              } else if (data.field === "email") {
                setTakenEmail(data.message);
              } else {
                setSubmitError("Something went wrog. Please try again");
              }
            });
          } else {
            dispatch(setUserUsername(username));
            history.push("/profile");
          }
        })
        .then(() => setIsSubmitting(false));
    }
  };

  if (loading) return <Loader />;

  return (
    <div className={classes.middle}>
      <Typography className={classes.header} variant="h3">
        EDIT YOUR MOODIE PROFILE
      </Typography>
      <div className={classes.EditProfileBox}>
        <Icon className={classes.profileImage} />
        <Formik
          enableReinitialize
          validateOnChange={false}
          validateOnBlur={false}
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={(values) => {
            attemptUpdate(values);
          }}
        >
          {({ values, errors }) => (
            <Form className={classes.form}>
              <TextField
                variant="outlined"
                margin="normal"
                fullWidth
                id="username"
                label="Username"
                name="username"
                InputLabelProps={{ shrink: true }}
                value={values.username}
                error={!!errors.username || takenUsername !== ""}
                helperText={errors.username || takenUsername}
                onChange={(e) => {
                  setUsername(e.target.value);
                }}
              />
              <TextField
                variant="outlined"
                margin="normal"
                style={{ width: "300px" }}
                id="email"
                label="Email"
                name="email"
                InputLabelProps={{ shrink: true }}
                value={values.email}
                error={!!errors.email || takenEmail !== ""}
                helperText={errors.email || takenEmail}
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
              />
              <TextField
                variant="outlined"
                margin="normal"
                fullWidth
                id="birthdate"
                label="Birthdate"
                name="birthdate"
                type="date"
                InputLabelProps={{
                  shrink: true,
                }}
                InputProps={{ inputProps: { max: today() } }}
                value={values.birthdate}
                error={!!errors.birthdate}
                helperText={errors.birthdate}
                onChange={(e) => {
                  setBirthdate(e.target.value);
                }}
              />
              {isSubmitting ? (
                <CircularProgress />
              ) : (
                <>
                  <Button
                    type="submit"
                    className={classes.button}
                    variant="outlined"
                    color="primary"
                  >
                    Save
                  </Button>
                  {submitError ? <div>{submitError}</div> : null}
                </>
              )}
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
};

export default EditProfile;
