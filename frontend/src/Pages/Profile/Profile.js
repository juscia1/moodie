import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { Typography, Button } from "@material-ui/core";
import Icon from "@material-ui/icons/PersonOutlineOutlined";
import useStyles from "./useStyles";
import { API_URL } from "../../config/api";
import { clearUser } from "../../state/actions/user";
import { clearToken } from "../../state/actions/token";
import DeleteModal from "../../components/DeleteModal";
import Loader from "../../components/Loader";

const Profile = () => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const token = useSelector((state) => state.token);
  const [user, setUser] = useState({
    username: null,
    email: null,
    birthdate: null,
  });
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const history = useHistory();

  const requestData = () => {
    setLoading(true);
    const url = `${API_URL}/api/users/profile`;
    const options = {
      method: "GET",
      headers: {
        "content-type": "application/json",
        Authorization: token,
      },
    };
    if (token !== 0) {
      fetch(url, options)
        .then((res) => res.json())
        .then((data) => {
          setUser({
            username: data.username,
            email: data.email,
            birthdate: data.birthdate,
          });
          setLoading(false);
        });
    }
  };

  useEffect(() => {
    requestData();
  }, []);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const DeleteAccount = () => {
    const url = `${API_URL}/api/users/`;
    const options = {
      method: "DELETE",
      headers: {
        "content-type": "application/json",
        Authorization: token,
      },
    };
    if (token !== 0) {
      fetch(url, options).then((response) => {
        if (response.ok) {
          sessionStorage.clear();
          dispatch(clearToken());
          dispatch(clearUser());
          history.push("/login");
        }
      });
    }
  };

  if (loading) return <Loader />;
  return (
    <div className={classes.middle}>
      <Typography className={classes.header} variant="h2">
        MY MOODIE PROFILE
      </Typography>
      <h4>
        Hey, {user.username}! We hope you&apos;re doing fine. Here you can take
        a look at your profile and edit it
      </h4>
      <div className={classes.profileBox}>
        <Icon className={classes.profileImage} />
        <h4>Username: {user.username}</h4>
        <h4>Email: {user.email}</h4>
        <h4>Birthdate: {user.birthdate}</h4>
        <div>
          <Link to="/EditProfile">
            <Button
              className={classes.button}
              variant="outlined"
              color="primary"
            >
              Edit
            </Button>
          </Link>
          <Button
            className={classes.button}
            variant="outlined"
            color="primary"
            onClick={handleOpen}
          >
            Delete account :(
          </Button>

          <DeleteModal
            open={open}
            handleClose={handleClose}
            del={DeleteAccount}
            mainText="Do your really want to delete your Moodie account?"
            secondaryText="You will lose all the data that you have entered."
          />
        </div>
      </div>
    </div>
  );
};

export default Profile;
