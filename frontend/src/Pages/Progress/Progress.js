import React from "react";
import { Typography } from "@material-ui/core";
import { ToggleButton, ToggleButtonGroup } from "@material-ui/lab";
import ProgChart from "../../components/ProgChart";
import useStyles from "./useStyles";

const Progress = () => {
  const [range, setRange] = React.useState("week");
  const classes = useStyles();
  const handleRange = (event, newRange) => {
    if (newRange != null) setRange(newRange);
  };

  return (
    <div>
      <Typography
        variant="h2"
        style={{ margin: "auto", textAlign: "center", paddingTop: 20 }}
      >
        MY PROGRESS
      </Typography>
      <div
        style={{
          float: "right",
          paddingRight: 69,
        }}
      >
        <ToggleButtonGroup
          size="small"
          value={range}
          exclusive="true"
          onChange={handleRange}
        >
          <ToggleButton
            className={classes.toggle}
            value="week"
            classes={{
              root: classes.root,
              selected: classes.selected,
            }}
          >
            Week
          </ToggleButton>
          <ToggleButton
            value="month"
            classes={{
              root: classes.root,
              selected: classes.selected,
            }}
          >
            Month
          </ToggleButton>
        </ToggleButtonGroup>
      </div>
      <ProgChart range={range} />
    </div>
  );
};

export default Progress;
