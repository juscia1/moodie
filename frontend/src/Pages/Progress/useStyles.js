import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(() => ({
  root: {
    "&$selected": {
      backgroundColor: "#d77fe7",
    },
  },
  selected: {},
}));

export default useStyles;
