import React, { useEffect, useState } from "react";
import {
  Button,
  CircularProgress,
  Grid,
  Hidden,
  Link,
  TextField,
  Typography,
} from "@material-ui/core";
import * as yup from "yup";
import { Form, Formik } from "formik";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import useStyles from "./useStyles";
import today from "../../utils/today";
import { API_URL } from "../../config/api";

const Register = () => {
  const [takenUsername, setTakenUsername] = useState("");
  const [takenEmail, setTakenEmail] = useState("");
  const [submitError, setSubmitError] = useState("");
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [successRegistration, setSuccessRegistration] = useState(false);
  const classes = useStyles();

  const { token, loading } = useSelector((state) => state);
  const { push } = useHistory();

  useEffect(() => {
    if (token) {
      push("/");
    }
  }, [token]);

  const validationSchema = yup.object({
    username: yup
      .string()
      .required("Username is required")
      .trim("Username cannot be blank")
      .strict(),
    email: yup
      .string()
      .email("Email must be a valid email")
      .required("Email is required")
      .strict(),
    birthdate: yup.date().required("Birthdate is required").nullable(),
    password: yup
      .string()
      .matches(
        "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})",
        "Password must contain at least 1 lowercase, 1 uppercase, and 1 numeric character and must be at least 8 characters or longer"
      )
      .required("Password is required"),
    confirmPassword: yup
      .string()
      .required("This field is required")
      .oneOf([yup.ref("password")], "Passwords don't match"),
  });

  const initialValues = {
    username: "",
    email: "",
    birthdate: "",
    password: "",
    confirmPassword: "",
  };

  const attemptRegister = (values) => {
    setTakenEmail("");
    setTakenUsername("");
    setIsSubmitting(true);
    const url = `${API_URL}/api/users/sign-up`;
    const options = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(values),
    };
    fetch(url, options).then((response) => {
      if (!response.ok) {
        response.json().then((data) => {
          if (data.field === "username") {
            setTakenUsername(data.message);
          } else if (data.field === "email") {
            setTakenEmail(data.message);
          } else {
            setSubmitError("Something went wrog. Please try again");
          }
          setIsSubmitting(false);
        });
      } else {
        setIsSubmitting(false);
        setSuccessRegistration(true);
      }
    });
  };

  if (loading) return <div />;
  return (
    <Grid container component="main" className={classes.root}>
      {successRegistration ? (
        <Grid item className={classes.paper}>
          <Typography variant="h4">
            REGISTRATION <span style={{ color: "#9c27b0" }}>SUCCESSFUL!</span>
            <br />
            <br />
          </Typography>
          <Typography variant="h5">
            {`PROCEED TO `}
            <Link href="/login">SIGN IN</Link>
          </Typography>
        </Grid>
      ) : (
        <>
          <Hidden only="xs">
            <Grid item sm={4} md={6}>
              <div className={classes.side}>
                <Typography variant="h5">
                  WHY DO I NEED A{" "}
                  <span style={{ color: "#9c27b0" }}>MOODIE</span> ACCOUNT?
                  <br />
                  <br />
                </Typography>
                <Typography>
                  AN ACCOUNT IS NECESSARY IN ORDER TO KEEP TRACK OF YOUR DAILY
                  MOOD AND SLEEP DATA AND GET PERSONALIZEED MENTAL HEALTH TIPS.
                </Typography>
              </div>
            </Grid>
          </Hidden>
          <Grid item xs={12} sm={8} md={6}>
            <div className={classes.paper}>
              <Typography variant="h5">
                CREATE A <span style={{ color: "#9c27b0" }}>MOODIE</span>{" "}
                ACCOUNT!
              </Typography>
              <Formik
                enableReinitialize
                validateOnChange={false}
                validateOnBlur={false}
                initialValues={initialValues}
                validationSchema={validationSchema}
                onSubmit={(values) => attemptRegister(values)}
              >
                {({ values, errors, handleChange }) => (
                  <Form className={classes.form}>
                    <TextField
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      id="username"
                      label="Username"
                      name="username"
                      value={values.username}
                      error={!!errors.username || takenUsername !== ""}
                      helperText={errors.username || takenUsername}
                      onChange={handleChange}
                    />
                    <TextField
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      id="email"
                      label="Email"
                      name="email"
                      value={values.email}
                      error={!!errors.email || takenEmail !== ""}
                      helperText={errors.email || takenEmail}
                      onChange={handleChange}
                    />
                    <TextField
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      id="birthdate"
                      label="Birthdate"
                      name="birthdate"
                      type="date"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      InputProps={{ inputProps: { max: today() } }}
                      value={values.birthdate}
                      error={!!errors.birthdate}
                      helperText={errors.birthdate}
                      onChange={handleChange}
                    />
                    <TextField
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      id="password"
                      label="Password"
                      name="password"
                      type="password"
                      value={values.password}
                      error={!!errors.password}
                      helperText={errors.password}
                      onChange={handleChange}
                    />
                    <TextField
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      id="confirmPassword"
                      label="Confirm Password"
                      name="confirmPassword"
                      type="password"
                      value={values.confirmPassword}
                      error={!!errors.confirmPassword}
                      helperText={errors.confirmPassword}
                      onChange={handleChange}
                    />
                    {isSubmitting ? (
                      <CircularProgress className={classes.submit} />
                    ) : (
                      <>
                        <Button
                          type="submit"
                          variant="outlined"
                          color="primary"
                          className={classes.submit}
                        >
                          Create
                        </Button>
                        {submitError ? <div>{submitError}</div> : null}
                      </>
                    )}
                  </Form>
                )}
              </Formik>
              <Typography>
                {"ALREADY A MEMBER? "}
                <Link href="/login">SIGN IN</Link>
              </Typography>
            </div>
          </Grid>
        </>
      )}
    </Grid>
  );
};
export default Register;
