import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh",
    alignItems: "center",
    display: "flex",
    justifyContent: "center",
  },
  paper: {
    margin: theme.spacing(4, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    textAlign: "center",
  },
  side: {
    margin: theme.spacing(1, 4),
    display: "flex",
    flexDirection: "column",
    textAlign: "left",
    justifyContent: "center",
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    margin: theme.spacing(2, 0, 0),
  },
  submit: {
    margin: theme.spacing(2, "auto"),
    width: "50%",
  },
}));

export default useStyles;
