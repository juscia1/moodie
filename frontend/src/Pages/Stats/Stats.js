import React, { useEffect, useState } from "react";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import {
  Typography,
  Button,
  ButtonGroup,
  CircularProgress,
} from "@material-ui/core";
import OpenIcon from "@material-ui/icons/Visibility";
import EditIcon from "@material-ui/icons/Create";
import DeleteIcon from "@material-ui/icons/DeleteOutline";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";
import useStyles from "./useStyles";
import { API_URL } from "../../config/api";
import DeleteModal from "../../components/DeleteModal";

const Stats = () => {
  const columns = [
    { id: "date", label: "Date" },
    { id: "mood", label: "Mood scale" },
    { id: "sleepHours", label: "Hours of sleep" },
    { id: "notes", label: "Notes" },
    { id: "buttons", label: "" },
  ];

  const [rows, setRows] = useState([]);
  const [loading, setLoading] = useState(false);
  const [open, setOpen] = useState();
  const [deleteId, setDeleteId] = useState();
  const [refreshData, setRefreshData] = useState(false);
  const { token } = useSelector((state) => state);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const history = useHistory();
  const buttons = (id) => {
    return (
      <>
        <ButtonGroup color="primary" variant="text">
          <Button onClick={() => history.push(`/metrics/${id}`)}>
            <OpenIcon />
          </Button>
          <Button onClick={() => history.push(`/metrics/edit/${id}`)}>
            <EditIcon />
          </Button>
          <Button
            onClick={() => {
              setDeleteId(id);
              handleOpen();
            }}
          >
            <DeleteIcon />
          </Button>
        </ButtonGroup>
      </>
    );
  };

  useEffect(() => {
    setLoading(true);
    const url = `${API_URL}/api/metrics`;
    const options = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: token,
      },
    };
    fetch(url, options)
      .then((response) => {
        if (!response.ok) {
          console.log("do smth");
        }
        /* eslint-disable */
        response.json().then((data) => {
          /* eslint-enable */
          setRows(data);
        });
      })
      .then(() => setLoading(false));
  }, [refreshData]);

  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const deleteSubmit = () => {
    const url = `${API_URL}/api/metrics/delete/${deleteId}`;
    const options = {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: token,
      },
    };
    fetch(url, options).then((response) => {
      if (response.ok) {
        handleClose();
      }
      setRefreshData(true);
    });
  };

  if (loading)
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          flexDirection: "column",
          alignItems: "center",
          width: "100vw",
          height: "100vh",
        }}
      >
        <CircularProgress color="primary" size="20%" />
      </div>
    );
  return (
    <>
      <Paper className={classes.middle}>
        <Typography className={classes.header} variant="h2">
          MY SUBMITS
        </Typography>
        <TableContainer className={classes.container}>
          <Table>
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell key={column.id} className={classes.column}>
                    {column.label}
                  </TableCell>
                ))}
                <TableCell key="collums" className={classes.column} />
              </TableRow>
            </TableHead>
            <TableBody>
              {rows
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row) => {
                  return (
                    <TableRow hover role="checkbox" tabIndex={-1}>
                      {columns.map((column) => {
                        const value = row[column.id];
                        return (
                          <TableCell
                            key={column.id}
                            align={column.align}
                            className={classes.tableData}
                          >
                            {column.format && typeof value === "number"
                              ? column.format(value)
                              : value}
                          </TableCell>
                        );
                      })}
                      <TableCell
                        key="buttons"
                        align="center"
                        className={classes.tableData}
                      >
                        {buttons(row.id)}
                      </TableCell>
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
      <DeleteModal
        open={open}
        handleClose={handleClose}
        del={deleteSubmit}
        mainText="Do your really want to delete your mood submit?"
        secondaryText="You will lose this day's data."
      />
    </>
  );
};

export default Stats;
