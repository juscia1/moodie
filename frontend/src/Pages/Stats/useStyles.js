import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  middle: {
    textAlign: "center",
    margin: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    paddingTop: "35px",
    borderWidth: "2px",
    borderColor: "rgba(156, 39, 176, 0.4)",
    borderStyle: "solid",
  },
  ViewSubmitBox: {
    background: "rgba(215, 127, 231, 0.1)",
    borderRadius: "50px",
    padding: "20px 10%",
    margin: "auto",
    width: "700px",
  },
  headerForView: {
    paddingBottom: "20px",
  },
  button: {
    margin: theme.spacing(2, 3),
    width: "15%",
  },
  column: {
    textAlign: "center",
    background: "rgba(156, 39, 176, 0.2)",
    fontSize: 16,
    fontWeight: "bold",
  },
  modalButton: {
    margin: theme.spacing(1, 5),
    width: "25%",
  },
  header: {
    paddingBottom: "35px",
    borderBottomWidth: "1px",
    borderBottomColor: "rgba(156, 39, 176, 0.4)",
    borderBottomStyle: "solid",
  },
  tableData: {
    textAlign: "center",
    paddingTop: "8px",
    paddingBottom: "8px",
  },
  paper: {
    position: "fixed",
    width: 400,
    backgroundColor: "#d77fe7",
    border: "2px solid #000",
    borderRadius: "25px",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    top: "35%",
    left: "39%",
  },
}));

export default useStyles;
