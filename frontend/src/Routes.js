import React from "react";
import { Switch, BrowserRouter as Router, Route } from "react-router-dom";
import Home from "./Pages/Home";
import Profile from "./Pages/Profile/Profile";
import Stats from "./Pages/Stats";
import EditProfile from "./Pages/Profile/EditProfile";
import Progress from "./Pages/Progress";
import MoodEdit from "./components/MoodEdit";
import MoodView from "./components/MoodView";

const Routes = () => {
  /* eslint-disable react/jsx-no-duplicate-props */
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route exact path="/stats" component={Stats} />
        <Route exact path="/profile" component={Profile} />
        <Route exact path="/editprofile" component={EditProfile} />
        <Route exact path="/progress" component={Progress} />
        <Route exact path="/home" component={Home} />
        <Route exact path="/metrics/:id" component={MoodView} />
        <Route exact path="/metrics/edit/:id" component={MoodEdit} />
        <Route path="/">
          <h1>404 PAGE NOT FOUND</h1>
        </Route>
      </Switch>
    </Router>
  );
};

export default Routes;
