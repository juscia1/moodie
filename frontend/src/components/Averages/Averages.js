import { Container, Grid, Hidden, Link, Typography } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { API_URL } from "../../config/api";
import Loader from "../Loader";
import useStyles from "./useStyles";
import logo from "./image.png";

const Averages = () => {
  const classes = useStyles();
  const [stats, setStats] = useState(null);
  const { token } = useSelector((state) => state);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    const url = `${API_URL}/api/metrics/averages`;
    const options = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: token,
      },
    };
    fetch(url, options).then((response) => {
      if (response.ok) {
        response.json().then((data) => {
          setStats(data);
          setLoading(false);
        });
      } else {
        setLoading(false);
        throw new Error("Something went wrong");
      }
    });
  }, [token]);

  if (loading) {
    return <Loader />;
  }
  return (
    <div className={classes.container}>
      <Container maxWidth="md" className={classes.center}>
        <Grid
          container
          spacing={0}
          direction="row"
          justify="center"
          alignItems="center"
        >
          <Grid item xs={6}>
            <Grid
              container
              spacing={1}
              direction="row"
              justify="center"
              alignItems="center"
            >
              <Grid item xs={12}>
                <Typography variant="h4">MOOD SUBMITTED</Typography>
              </Grid>
              <Grid item xs={12}>
                <Typography variant="h6">
                  {"You can check your "}
                  <Link href="/stats">stats</Link>
                  {" or your "}
                  <Link href="/progress">progress</Link>
                </Typography>
              </Grid>
              <Grid item xs={12} sm={6}>
                <Grid
                  container
                  direction="column"
                  justify="center"
                  alignItems="center"
                  spacing={1}
                >
                  <Grid item>
                    <Typography variant="h6">
                      <span style={{ color: "#9c27b0", fontWeight: "bold" }}>
                        Week
                      </span>{" "}
                      averages
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Typography>
                      Mood: {stats ? stats.value0.mood : ""}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Typography>
                      Sleep: {stats ? stats.value0.sleepHours : ""}
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={12} sm={6}>
                <Grid
                  container
                  direction="column"
                  justify="center"
                  alignItems="center"
                  spacing={1}
                >
                  <Grid item>
                    <Typography variant="h6">
                      <span style={{ color: "#9c27b0", fontWeight: "bold" }}>
                        Month
                      </span>{" "}
                      averages
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Typography>
                      Mood: {stats ? stats.value1.mood : ""}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Typography>
                      Sleep: {stats ? stats.value1.sleepHours : ""}
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
              {stats && (
                <Grid item xs={12}>
                  {stats.value2.length > 0 ? (
                    stats.value2.map((warning) => (
                      <Typography>{warning}</Typography>
                    ))
                  ) : (
                    <Typography>You&apos;re doing great!</Typography>
                  )}
                </Grid>
              )}
            </Grid>
          </Grid>
          <Hidden only={["xs", "sm"]}>
            <Grid item md={6}>
              <img className={classes.image} src={logo} alt="logo" />
            </Grid>
          </Hidden>
        </Grid>
      </Container>
    </div>
  );
};

export default Averages;
