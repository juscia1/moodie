import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(() => ({
  center: {
    display: "flex",
    marginTop: "10vh",
    textAlign: "center",
  },
  image: {
    width: "60%",
    alignItems: "center",
  },
  container: {
    display: "flex",
    justifyContent: "center",
    position: "relative",
  },
}));

export default useStyles;
