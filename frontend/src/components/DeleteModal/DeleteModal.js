import { Button, Modal } from "@material-ui/core";
import React from "react";
import PropTypes from "prop-types";
import useStyles from "./useStyles";

const DeleteModal = ({ open, handleClose, del, mainText, secondaryText }) => {
  const classes = useStyles();
  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-title"
      aria-describedby="modal-description"
      className={classes.middle}
    >
      <div className={classes.paper}>
        <h2 id="modal-title">{mainText}</h2>
        <p id="modal-description">{secondaryText}</p>
        <div className={classes.buttonDiv}>
          <Button
            className={classes.modalButton}
            variant="outlined"
            onClick={del}
          >
            Yes
          </Button>
          <Button
            className={classes.modalButton}
            variant="outlined"
            onClick={handleClose}
          >
            No
          </Button>
        </div>
      </div>
    </Modal>
  );
};

DeleteModal.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  del: PropTypes.func.isRequired,
  mainText: PropTypes.string.isRequired,
  secondaryText: PropTypes.string.isRequired,
};

export default DeleteModal;
