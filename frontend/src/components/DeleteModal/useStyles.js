import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  middle: {
    display: "flex",
    padding: theme.spacing(1),
    alignItems: "center",
    justifyContent: "center",
  },
  profileBox: {
    background: "rgba(215, 127, 231, 0.1)",
    borderRadius: "50px",
    padding: "20px 10%",
    margin: "auto",
  },
  EditProfileBox: {
    background: "rgba(215, 127, 231, 0.1)",
    borderRadius: "50px",
    padding: "20px 10%",
    margin: "auto",
    width: "700px",
  },
  header: {
    paddingTop: "20px",
  },
  profileImage: {
    width: "160px",
    height: "160px",
    borderRadius: "50%",
    borderColor: "black",
    paddingBottom: "5px",
  },
  button: {
    margin: theme.spacing(2, "auto"),
    width: "75%",
  },
  buttonDiv: {
    display: "flex",
    flexDirection: "row",
  },
  modalButton: {
    margin: "auto",
    width: "25%",
  },
  paper: {
    width: 400,
    backgroundColor: "#d77fe7",
    border: "2px solid #000",
    borderRadius: "25px",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    top: "35%",
    left: "39%",
  },
}));

export default useStyles;
