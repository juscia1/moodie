import { CircularProgress } from "@material-ui/core";
import React from "react";

const Loader = () => {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        flexDirection: "column",
        alignItems: "center",
        width: "100vw",
        height: "100vh",
      }}
    >
      <CircularProgress color="primary" size="20%" />
    </div>
  );
};
export default Loader;
