import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  middle: {
    textAlign: "center",
    margin: theme.spacing(1, 4),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
  },
  happyImage: {
    width: "50px",
    height: "50px",
    borderRadius: "50%",
    borderColor: "black",
    paddingBottom: "10px",
    position: "absolute",
    right: "19%",
    top: "18%",
  },
  sadImage: {
    width: "50px",
    height: "50px",
    borderRadius: "50%",
    borderColor: "black",
    paddingBottom: "10px",
    position: "absolute",
    left: "19%",
    top: "18%",
  },
  button: {
    margin: theme.spacing(2, "auto"),
    width: "14%",
  },
  textField: {
    margin: theme.spacing(1, "auto"),
    width: "45%",
    display: "flex",
  },
  slider: {
    margin: theme.spacing(1, "auto"),
    width: "55%",
  },
  buttonGroup: {
    margin: theme.spacing(1, "auto"),
  },
}));

export default useStyles;
