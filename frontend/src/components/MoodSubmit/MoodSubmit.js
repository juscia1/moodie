import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import * as yup from "yup";
import { Form, Formik } from "formik";
import Slider from "@material-ui/core/Slider";
import Icon from "@material-ui/icons/SentimentVerySatisfied";
import Icon1 from "@material-ui/icons/SentimentVeryDissatisfied";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { CircularProgress } from "@material-ui/core";
import useStyles from "./useStyles";
import { API_URL } from "../../config/api";
import Loader from "../Loader";
import Averages from "../Averages";

const MoodSubmit = () => {
  const classes = useStyles();
  const username = useSelector((state) => state.user.username);
  function valuetext(value) {
    return `${value}`;
  }
  const [mood, setMood] = useState(5);
  const [sleepHours, setSleepHours] = useState(5);
  const [notes, setNotes] = useState("");
  const { token } = useSelector((state) => state);
  const [loading, setLoading] = useState(false);
  const [submitted, setSubmitted] = useState(false);
  const [checkingIfSubmitted, setCheckingIfSubmitted] = useState(true);

  useEffect(() => {
    const url = `${API_URL}/api/metrics/submitted`;
    const options = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: token,
      },
    };
    if (token) {
      fetch(url, options).then((response) => {
        response.json().then((data) => {
          setSubmitted(data);
          setCheckingIfSubmitted(false);
        });
      });
    }
  }, [token]);

  const handleIncrement = () => {
    setSleepHours(sleepHours === 24 ? sleepHours : sleepHours + 1);
  };
  const handleDecrement = () => {
    setSleepHours(sleepHours === 0 ? sleepHours : sleepHours - 1);
  };

  const validationSchema = yup.object({
    notes: yup.string(),
  });

  const attemptSubmit = (values) => {
    setLoading(true);
    const url = `${API_URL}/api/metrics`;
    const options = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: token,
      },
      body: JSON.stringify(values),
    };
    fetch(url, options)
      .then((response) => {
        if (response.ok) {
          setSubmitted(true);
        }
      })
      .then(() => setLoading(false));
  };

  const initialValues = {
    mood,
    sleepHours,
    notes,
  };

  if (checkingIfSubmitted) return <Loader />;
  if (submitted) return <Averages />;
  return (
    <div className={classes.middle}>
      <h2>Hey, {username}, how are you feeling today?</h2>
      <Formik
        enableReinitialize
        validateOnChange={false}
        validateOnBlur={false}
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={(values) => attemptSubmit(values)}
      >
        {({ values }) => (
          <Form className={classes.form}>
            <Slider
              id="mood"
              className={classes.slider}
              value={values.mood}
              getAriaValueText={valuetext}
              valueLabelDisplay="auto"
              step={1}
              marks
              min={0}
              max={10}
              onChange={(e, val) => {
                setMood(val);
              }}
            />
            <Icon className={classes.happyImage} />
            <Icon1 className={classes.sadImage} />
            <h2>How many hours of sleep did you get tonight?</h2>
            <ButtonGroup size="small" className={classes.buttonGroup}>
              <Button color="primary" onClick={handleDecrement}>
                -
              </Button>
              <Button id="sleepHours" color="primary" disabled>
                {sleepHours}
              </Button>
              <Button color="primary" onClick={handleIncrement}>
                +
              </Button>
            </ButtonGroup>
            <h2>Do you have anything else to add?</h2>
            <TextField
              className={classes.textField}
              id="notes"
              variant="outlined"
              multiline
              rows={5}
              value={values.notes}
              onChange={(e) => setNotes(e.target.value)}
            />
            {loading ? (
              <CircularProgress className={classes.button} />
            ) : (
              <Button
                className={classes.button}
                variant="outlined"
                color="primary"
                type="submit"
              >
                Submit
              </Button>
            )}
          </Form>
        )}
      </Formik>
    </div>
  );
};
export default MoodSubmit;
