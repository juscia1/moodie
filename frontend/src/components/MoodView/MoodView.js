import React, { useEffect, useState } from "react";
import { Typography, Button } from "@material-ui/core";
import { useHistory, useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import { API_URL } from "../../config/api";

import useStyles from "./useStyles";
import DeleteModal from "../DeleteModal";
import Loader from "../Loader";

const MoodView = () => {
  const { id } = useParams();
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const token = useSelector((state) => state.token);
  const [submit, setSubmit] = useState({
    date: null,
    mood: null,
    sleepHours: null,
    notes: null,
  });
  const history = useHistory();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const url = `${API_URL}/api/metrics/${id}`;
    const options = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: token,
      },
    };
    fetch(url, options).then((response) => {
      if (!response.ok) {
        console.log("do smth");
      }
      response.json().then((data) => {
        console.log(data);
        setSubmit({
          date: data.date,
          mood: data.mood,
          sleepHours: data.sleepHours,
          notes: data.notes,
        });
        setLoading(false);
      });
    });
  }, []);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const deleteSubmit = () => {
    const url = `${API_URL}/api/metrics/delete/${id}`;
    const options = {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: token,
      },
    };
    fetch(url, options).then((response) => {
      if (response.ok) {
        handleClose();
        history.push(`/stats`);
      }
    });
  };

  if (loading) return <Loader />;

  return (
    <div className={classes.middle}>
      <Typography className={classes.headerForView} variant="h3">
        THIS IS YOUR SUBMIT
      </Typography>
      <div className={classes.ViewSubmitBox}>
        <h4>Date: {submit.date}</h4>
        <h4>Mood scale: {submit.mood}</h4>
        <h4>Hours of sleep: {submit.sleepHours}</h4>
        <h4>Notes: {submit.notes}</h4>
      </div>
      <div>
        <Button
          className={classes.button}
          variant="outlined"
          color="primary"
          onClick={() => history.push(`/metrics/edit/${id}`)}
        >
          Edit
        </Button>
        <Button
          className={classes.button}
          variant="outlined"
          color="primary"
          onClick={handleOpen}
        >
          Delete
        </Button>

        <DeleteModal
          open={open}
          handleClose={handleClose}
          del={deleteSubmit}
          mainText="Do your really want to delete your mood submit?"
          secondaryText="You will lose this day's data."
        />
      </div>
    </div>
  );
};

export default MoodView;
