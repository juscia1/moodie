import {
  AppBar,
  Toolbar,
  Typography,
  makeStyles,
  Button,
  IconButton,
  Drawer,
  Link,
  MenuItem,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { API_URL } from "../../config/api";
import { clearToken } from "../../state/actions/token";
import { clearUser } from "../../state/actions/user";

const headersData = [
  {
    label: "Home",
    href: "/",
  },
  {
    label: "Stats",
    href: "/stats",
  },
  {
    label: "Progress",
    href: "/progress",
  },
  {
    label: "Profile",
    href: "/profile",
  },
];

const useStyles = makeStyles(() => ({
  header: {
    backgroundColor: "#d77fe7",
    paddingRight: "79px",
    paddingLeft: "118px",
    "@media (max-width: 900px)": {
      paddingLeft: 0,
    },
  },
  logo: {
    fontWeight: 600,
    color: "#FFFFFF",
    textAlign: "left",
  },
  menuButton: {
    fontWeight: 700,
    size: "18px",
    marginLeft: "38px",
  },
  toolbar: {
    display: "flex",
    justifyContent: "space-between",
  },
  drawerContainer: {
    padding: "20px 30px",
  },
}));

export default function Header() {
  const { header, logo, menuButton, toolbar, drawerContainer } = useStyles();
  const dispatch = useDispatch();

  const [state, setState] = useState({
    mobileView: false,
    drawerOpen: false,
  });
  const { mobileView, drawerOpen } = state;

  const logout = () => {
    const url = `${API_URL}/api/users/log-out`;
    const options = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    };
    fetch(url, options).then((response) => {
      if (response.ok) {
        sessionStorage.clear();
        dispatch(clearToken());
        dispatch(clearUser());
      }
    });
  };

  useEffect(() => {
    const setResponsiveness = () => {
      return window.innerWidth < 900
        ? setState((prevState) => ({ ...prevState, mobileView: true }))
        : setState((prevState) => ({ ...prevState, mobileView: false }));
    };
    setResponsiveness();
    window.addEventListener("resize", () => setResponsiveness());
  }, []);

  const logoutButton = () => {
    return (
      <Button className={menuButton} color="inherit" onClick={logout}>
        Log out
      </Button>
    );
  };

  /* eslint-disable */
  const logoutDrawerButton = () => {
    return (
      <Link onClick={logout}>
        <MenuItem>Log out</MenuItem>
      </Link>
    );
  };

  const getMenuButtons = () => {
    return headersData.map(({ label, href }) => {
      return (
        <Button href={href} className={menuButton} color="inherit" key={label}>
          {label}
        </Button>
      );
    });
  };

  const moodie = (
    <Typography variant="h6" component="h1" className={logo}>
      Moodie
    </Typography>
  );

  const displayDesktop = () => {
    return (
      <Toolbar className={toolbar}>
        {moodie}
        <div>
          {getMenuButtons()}
          {logoutButton()}
        </div>
      </Toolbar>
    );
  };

  const getDrawerChoices = () => {
    return headersData.map(({ label, href }) => {
      return (
        <Link href={href} key={label}>
          <MenuItem>{label}</MenuItem>
        </Link>
      );
    });
  };

  const displayMobile = () => {
    const handleDrawerOpen = () =>
      setState((prevState) => ({ ...prevState, drawerOpen: true }));
    const handleDrawerClose = () =>
      setState((prevState) => ({ ...prevState, drawerOpen: false }));

    return (
      <Toolbar>
        <IconButton
          aria-label="menu"
          arial-aria-haspopup="true"
          onClick={handleDrawerOpen}
        >
          <MenuIcon />
        </IconButton>
        <Drawer anchor="left" open={drawerOpen} onClose={handleDrawerClose}>
          <div className={drawerContainer}>
            {getDrawerChoices()}
            {logoutDrawerButton()}
          </div>
        </Drawer>
        {moodie}
      </Toolbar>
    );
  };

  return (
    <AppBar className={header} position="sticky">
      {mobileView ? displayMobile() : displayDesktop()}
    </AppBar>
  );
}
