import React, { useEffect, useState } from "react";
import { Line, defaults } from "react-chartjs-2";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";
import _ from "lodash";
import { CircularProgress } from "@material-ui/core";
import { API_URL } from "../../config/api";
import useStyles from "./useStyles";

const ProgChart = ({ range }) => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const { token } = useSelector((state) => state);
  const classes = useStyles();
  const [state, setState] = useState({
    mobileView: false,
  });
  const { mobileView } = state;
  useEffect(() => {
    const setResponsiveness = () => {
      return window.innerWidth < 900
        ? setState((prevState) => ({ ...prevState, mobileView: true }))
        : setState((prevState) => ({ ...prevState, mobileView: false }));
    };
    setResponsiveness();
    window.addEventListener("resize", () => setResponsiveness());
    setLoading(true);
    const url = `${API_URL}/api/metrics/progress`;
    const options = {
      method: "GET",
      headers: {
        "content-type": "application/json",
        Authorization: token,
        Range: range,
      },
    };
    fetch(url, options).then((response) =>
      response
        .json()
        .then((result) => {
          setData({
            labels: _.map(result, "date"),
            datasets: [
              {
                label: "Sleep (hours)",
                data: _.map(result, "sleepHours"),
                fill: true,
                backgroundColor: "rgba(215, 127, 231, 0.1)",
                borderColor: "rgba(215, 127, 231, 1)",
              },
              {
                label: "Mood (1-10)",
                data: _.map(result, "mood"),
                fill: false,
                borderColor: "#742774",
              },
            ],
          });
        })
        .then(() => setLoading(false))
    );
  }, [range]);
  defaults.plugins.legend.align = "start";
  defaults.layout.padding = { top: 0, bottom: 10, right: 69, left: 69 };
  if (loading) {
    return (
      <div
        style={{
          display: "flex",
          marginTop: 100,
          justifyContent: "center",
          alignItems: "center",
          textAlign: "center",
        }}
      >
        <CircularProgress color="primary" size="30%" />
      </div>
    );
  }
  const displayMobile = () => {
    return (
      <Line
        data={data}
        width="80vh"
        height="90vh"
        options={
          ({
            maintainAspectRatio: false,
          },
          {
            tooltips: {
              enabled: false,
            },
          },
          {
            scales: {
              y: {
                min: 0,
              },
            },
          })
        }
      />
    );
  };
  const displayDesktop = () => {
    return (
      <Line
        data={data}
        width="80vh"
        height="26vh"
        options={
          ({
            maintainAspectRatio: false,
          },
          {
            tooltips: {
              enabled: false,
            },
          },
          {
            scales: {
              y: {
                min: 0,
              },
            },
          })
        }
      />
    );
  };
  return (
    <div className={classes.wrapper}>
      {mobileView ? displayMobile() : displayDesktop()}
    </div>
  );
};

ProgChart.propTypes = {
  range: PropTypes.string.isRequired,
};

export default ProgChart;
