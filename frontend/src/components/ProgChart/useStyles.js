import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(() => ({
  canvas: {
    width: "10px !important",
    height: "60px !important",
  },
}));
export default useStyles;
