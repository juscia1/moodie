/* eslint-disable */
export const SET_USER = "user/SET_USER";
export const CLEAR_USER = "user/CLEAR_USER";
export const SET_USERNAME = "user/SET_USERNAME";

export const SET_TOKEN = "token/SET_TOKEN";
export const CLEAR_TOKEN = "token/CLEAR_TOKEN";

export const SET_LOADING = "loading/SET_LOADING";
