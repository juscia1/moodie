import { SET_LOADING } from "../actionTypes";

/* eslint-disable */
export const setLoading = (bool) => ({
  type: SET_LOADING,
  payload: bool,
});
