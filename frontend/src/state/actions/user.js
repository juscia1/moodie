import { CLEAR_USER, SET_USER, SET_USERNAME } from "../actionTypes";

/* eslint-disable */
export const setUser = (user) => ({
  type: SET_USER,
  payload: user,
});

export const clearUser = () => ({
  type: CLEAR_USER,
  payload: {
    username: null,
    id: null,
  },
});

export const setUserUsername = (username) => ({
  type: SET_USERNAME,
  payload: username,
});
