import { SET_LOADING } from "../actionTypes";

const initlialState = false;

export default function userReducer(state = initlialState, action = {}) {
  switch (action.type) {
    case SET_LOADING:
      return action.payload;
    default:
      return state;
  }
}
