import { CLEAR_TOKEN, SET_TOKEN } from "../actionTypes";

const initlialState = 0;

export default function userReducer(state = initlialState, action = {}) {
  switch (action.type) {
    case SET_TOKEN:
      return action.payload;
    case CLEAR_TOKEN:
      return action.payload;
    default:
      return state;
  }
}
