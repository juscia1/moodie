import { CLEAR_USER, SET_USER, SET_USERNAME } from "../actionTypes";

const initlialState = null;

export default function userReducer(state = initlialState, action = {}) {
  switch (action.type) {
    case SET_USER:
      return action.payload;
    case CLEAR_USER:
      return action.payload;
    case SET_USERNAME:
      return {
        ...state,
        username: action.payload,
      };
    default:
      return state;
  }
}
