import { createStore, compose } from "redux";

import reducers from "./reducers";
import { loadFromLocalStorage, saveToLocalStorage } from "./util";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const user = {
  user: {
    ...loadFromLocalStorage("user"),
  },
};

const store = createStore(reducers, user, composeEnhancers());

store.subscribe(() => saveToLocalStorage("user", store.getState().user));

export default store;
