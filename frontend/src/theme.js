import { createMuiTheme } from "@material-ui/core";

const theme = createMuiTheme({
  palette: {
    primary: { main: "#9c27b0" },
  },
  overrides: {
    MuiButton: {
      root: {
        borderRadius: "25px",
      },
    },
    MuiOutlinedInput: {
      root: {
        borderRadius: "25px",
        "&:hover:not($disabled):not($focused):not($error) $notchedOutline": {
          borderColor: "#9c27b0",
        },
      },
    },
  },
});

export default theme;
