import { API_URL } from "../config/api";
import { setLoading } from "../state/actions/loading";
import { clearToken, setToken } from "../state/actions/token";
import { clearUser } from "../state/actions/user";

const refresh = (dispatch) => {
  dispatch(setLoading(true));
  const url = `${API_URL}/api/users/refresh`;
  const options = {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  };
  fetch(url, options)
    .then((response) => {
      if (response.ok) {
        dispatch(setToken(response.headers.get("Authorization")));
        dispatch(setLoading(false));
        setTimeout(() => {
          refresh();
        }, 1000 * 60 * 10);
      } else {
        throw new Error();
      }
    })
    .catch(() => {
      dispatch(clearToken());
      dispatch(clearUser());
      dispatch(setLoading(false));
    });
};

export default refresh;
